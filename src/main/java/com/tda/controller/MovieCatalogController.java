package com.tda.controller;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.tda.models.MovieCatalog;
import com.tda.models.MovieInfo;
import com.tda.models.MovieRatingInfo;
 	
@RestController
@RequestMapping("/catalog")
public class MovieCatalogController {
	
	@RequestMapping("/{userId}")
	public 	List<MovieCatalog> getMovieList(@PathVariable("userId")String userId) {
		
		RestTemplate restTemplate=new RestTemplate();
		
		List<MovieRatingInfo> rating=Arrays.asList(new MovieRatingInfo("m102",3.9),new MovieRatingInfo("m103",3.2));
		
		return rating.stream().map(ratings ->{
			MovieInfo movie=restTemplate.getForObject("http://localhost:8011/movie/"+ratings.getMovieId(),MovieInfo.class);
		return new MovieCatalog(movie.getMovieName(),"Epic movie about avengers",ratings.getRating());
		}).collect(Collectors.toList());
			
	}

}
