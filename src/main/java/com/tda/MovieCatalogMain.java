package com.tda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieCatalogMain {

	public static void main(String[] args) {
		SpringApplication.run(MovieCatalogMain.class, args);
	}

}
