package com.tda.models;

public class MovieRatingInfo {
private String movieId;
private double rating;

public MovieRatingInfo() {
	
}
public MovieRatingInfo(String movieId, double rating) {
	this.movieId = movieId;
	this.rating = rating;
}
public String getMovieId() {
	return movieId;
}
public void setMovieId(String movieId) {
	this.movieId = movieId;
}
public double getRating() {
	return rating;
}
public void setRating(double rating) {
	this.rating = rating;
}
@Override
public String toString() {
	return "MovieRatingInfo [movieId=" + movieId + ", rating=" + rating + "]";
}


}
