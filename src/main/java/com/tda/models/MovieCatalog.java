package com.tda.models;

//import lombok.Data;
//import lombok.ToString;
//
//@Data
//@ToString
public class MovieCatalog {
	private String movieName;
	private String Desc;
	private double Rating;
	
	public MovieCatalog() {
		
	}
	
	
	public MovieCatalog(String movieName, String desc, double rating) {
		this.movieName = movieName;
		Desc = desc;
		Rating = rating;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public String getDesc() {
		return Desc;
	}
	public void setDesc(String desc) {
		Desc = desc;
	}
	public double getRating() {
		return Rating;
	}
	public void setRating(double rating) {
		Rating = rating;
	}
	@Override
	public String toString() {
		return "MovieCatalog [movieName=" + movieName + ", Desc=" + Desc + ", Rating=" + Rating + "]";
	}
	
	

}
