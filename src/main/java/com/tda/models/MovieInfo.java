package com.tda.models;

public class MovieInfo {
private String Id;
private  String movieName;
public MovieInfo() {
	
}
public MovieInfo(String id, String movieName) {
	Id = id;
	this.movieName = movieName;
}
public String getId() {
	return Id;
}
public void setId(String id) {
	Id = id;
}
public String getMovieName() {
	return movieName;
}
public void setMovieName(String movieName) {
	this.movieName = movieName;
}
@Override
public String toString() {
	return "MovieInfo [Id=" + Id + ", movieName=" + movieName + "]";
}

}
